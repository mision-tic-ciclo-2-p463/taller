/*
*Autor:
*Fecha:
*Empresa:
*Ciudad:
*Descripcion del proyecto:
*/
public class Empleado {
    private String  nombre;
    private String apellido;
    private String cedula;
    private double sueldo;

    public Empleado(String nombre, String apellido, String cedula){
        this.nombre=nombre;
        this.apellido=apellido;
        this.cedula=cedula;

    }
    /**
     * consultores getter
     * 
     */
    public String getNombre(){
        return nombre;
    }
    public String getApellido(){
        return apellido;
    }
}
