import java.util.ArrayList;
import java.util.List;

public class Recepcionista extends Empleado{

    private List<Cliente> clientes;

    public Recepcionista(String nombre, String apellido, String cedula){
        super(nombre, apellido, cedula);
        this.clientes=new ArrayList<Cliente>();
    }

    public Cliente getCliente(int pos){
        return this.clientes.get(pos);
    }
    public void registrar_clientes(String nombre, String apellido, String cedula){
        Cliente objCliente=new Cliente(nombre, apellido, cedula);
        this.clientes.add(objCliente);
    }
    public void registrar_vehiculos(){

    }
}
